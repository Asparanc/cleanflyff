//|][: ~ %&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#
/*
															`.-`
													   .:/+oo++oo+/:-
													.+sso++++///+++oss+-
						 .                       `./yysooo++o--o+++oosyh+:-..``             ``.-://.
						 shysoo+/:--..--:/++oshdmmmhyso/.-+syhhhs+-.:osyhdmmddhyysoo+////+osyyyhmmmy`
						sdmmdyo+ooo+++++ooossssyyyhhy/. `:oshddhso:` `:syhossoooo+++++///////ohhydsyy`
					  `sy+sh+syy+/:------::::://::hys/-` .:+osso+/- `-/syh:.::::::--....--/oys//ho//oy.
					 `ys///ss///ss+:-.....---:::`+hhyyss+///+++++//+ssyyhhs `:::--.....-:os+::/so/::/oy:
					.yo/:::/s+::::+s+:.....--:-  /hhhyyyssoooooooosssyyhhho  `-:--....-+o/---:/s/----:+s+.
				   :s+:----:+s/--..-/o/-...--:`   ohhhyyyyssssysssyyyyhhhs`   `:--..-/o/.....-s/-..`...-/o+-
				 :o+:-..``..-oo-.....-/+/:--::    `shhhhhhhhhyyhhhhyhhhhy`     ::::/+/-..```./o-.````...-://::.
			   -++/-...````..:o:.```...-/+///:     `/yhhhhhhyyyyhhhhhhh+.      ://++:--.....-o:......--..``  `.
			 :/--------......./+.....--::/+++:       `/yhhhhhhyhhhhhy/`        :++/:::------/+-----.`
			``       ``..-----:+:-::-.....--:-          .:/yhhhhy/:.           `.````   .-::+/::.`
						  `.-::++::.                       hNmmm.                         `:+-`
							  .:+.                         mmmmo
														  `mmmd`
														  -mmm+
														   yhs
														  `hh/
														  `hh-
														  `yy`
														   os.
														  :s-
														  .o-
														   o:
														   -+.`-`
															`//so:`
															  .++:-`
															   .o+:-`
																os+/
																 -/-
*/
//|][: ~ %&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#

#define NEUZ_TITLE	"Ketchup V18 (VS19)"
#define NEUZ_IPSET	"127.0.0.1"
#define NEUZ_CPORT	"5400"
#define NEUZ_BHASH	"sunkist"
#define NEUZ_PHASH	"kikugalanet"
#define NEUZ_MSGVR  "20100412"

//|][: ~ %&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#

#define	__ANGEL_CREATION_FIX
#define	__ARROW_FIX
#define	__AUTO_SKILL_MAX
#define	__BS_FIX_ARRIVEPOS_ALGO
#define	__FAST_NEUZ
#define	__FIX_RANGER_ROLLER
#define	__FIX_SETFST
#define	__INVENTORY_168
#define	__INVENTORY_SORTING
#define	__LOOKCHANGE
#define	__NEW_EXCHANGE_V19
#define	__NOACCT_LOCK
#define	__PMA_FIX_ATKMSG
#define	__POSI_BUG_FIX
#define	__RANDOMOPTION_RETRY
#define	__SLEEPING_FIX
#define	__STAT_SYNC
#define	__SYS_SCR_CANCELLATION
#define	__SYS_SCR_PARTYPOINTFREE
#define	__SYS_SCR_PARTYSPEEDLEVELUP
#define	__SYS_SYS_ARMOREFFECTCHANGE
#define	__SYS_SYS_ITEMMODELRETURN
#define	__TREASUREBOX
#define	__V16_FORMATS
#define	__V19_FORMATS
#define	__V21_GM_COMMANDS
#define	__V21_LEVELUP_SFX
#define	__V21_PREMIUM_BATTERY
#define	__V21_SIEGE_BUFFS
#define	__YS_CHATTING_BLOCKING_SYSTEM
#define	__ZONE_AFFICHAGE

//|][: ~ %&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#%&#