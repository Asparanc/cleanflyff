/*
Event	500
{
	Title
	{
		LANG_KOR	발렌타인 2007
		LANG_USA
		LANG_PHP
		LANG_ID
		LANG_JAP
		LANG_CHI
		LANG_THA
		LANG_TWN
		LANG_GER
	}
	Time
	{
		2007	2	13	10
		2007	2	27	10
	}
	Item
	{
		II_SYS_SYS_SCR_VALENTINE	10000	1
	}
}
*/

Event	531	// 출첵 이벤트							
{									
	Title								
	{								
		LANG_KOR	신년 출석 이벤트
		//LANG_USA							
		//LANG_PHP							
		//LANG_ID							
		//LANG_JAP							
		//LANG_CHI							
		//LANG_THA							
		//LANG_TWN							
		//LANG_GER							
		//LANG_FRE							
	}								
	EventLimitLevel								
	{								
		15							
	}								
	CallTheRollItem								
	{								
		1	II_SYS_SYS_SCR_PUMKSOUP_N	5	0
		1	II_SYS_SYS_SCR_URICNOODLE_N	5	0
		2	II_SYS_SYS_SCR_CARD_2011_N	5	0
		2	II_SYS_SYS_SCR_CARD_2011FWC_N	5	0
		3	II_GEN_POT_MASTER_WAR_1_N	3	0
		3	II_GEN_POT_MASTER_MAG_1_N	3	0
		3	II_GEN_POT_MASTER_ARC_1_N	3	0
		4	II_SYS_SYS_SCR_KING_BUFF_N	3	0
		4	II_SYS_SYS_SCR_QUEEN_BUFF_N	5	0
		5	II_SYS_SYS_SCR_JACK_BUFF_N	2	0
		5	II_SYS_SYS_SCR_ACE_BUFF_N	5	0
	}								
	Time								
	{								
		2018	12	26	00				
		2019	01	03	10				
	}								
}									







Event	501
{
	Title
	{
		LANG_KOR	찾아라!! 보물지도!!
		LANG_USA
		LANG_PHP
		LANG_ID
		LANG_JAP
		LANG_CHI
		LANG_THA
		LANG_TWN
		LANG_GER
	}
	Time
	{
		2009	4	3	10
		2009	4	28	00
	}
	Spawn
	{
		OT_ITEM	II_SYS_SYS_EVE_WORLDMAP01	2300	1.5
	}
}

Event	502
{
	Title
	{
		LANG_KOR	
		LANG_USA
		LANG_PHP
		LANG_ID
		LANG_JAP
		LANG_CHI
		LANG_THA
		LANG_TWN
		LANG_GER
	}
	Time
	{
		2009	4	3	10
		2009	4	28	00
	}
	Spawn
	{
		OT_ITEM	II_SYS_SYS_EVE_WORLDMAP02	2400	1.5
	}
}