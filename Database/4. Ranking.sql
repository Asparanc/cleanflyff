USE [RANKING_DBF]
GO
DROP TABLE RANKING_TBL
GO
DROP PROCEDURE RANKING_STR
GO
CREATE TABLE [dbo].[RANKING_TBL] (
  [m_dwLogo] int NOT NULL,
  [m_szGuild] varchar(255) COLLATE Latin1_General_BIN NOT NULL,
  [m_szName] varchar(255) COLLATE Latin1_General_BIN NOT NULL,
  [m_nWin] int NOT NULL,
  [m_nLose] int NOT NULL,
  [m_nSurrender] int NOT NULL,
  [m_AvgLevel] float NOT NULL,
  [m_nWinPoint] int NOT NULL,
  [Gu] char(2) COLLATE Latin1_General_BIN CONSTRAINT [DF__GUILDSRANKIN__Gu__0519C6AF] DEFAULT 'R1' NULL,
  [serverindex] char(2) COLLATE Latin1_General_BIN CONSTRAINT [DF__GUILDSRAN__serve__060DEAE8] DEFAULT '01' NULL,
  [s_date] char(10) COLLATE Latin1_General_BIN NULL,
  [m_idGuild] char(6) COLLATE Latin1_General_BIN NULL,
  [m_MaximumUnity] float NULL,
  [CreateTime] datetime NULL,
  [m_nGuildGold] int NULL,
  [m_nPlayTime] int NULL
)
ON [PRIMARY]

GO
CREATE       PROC [dbo].RANKING_STR
@Gu				CHAR(2) = 'R1',
@iserverindex	CHAR(2) = '01'
AS
set nocount on
DECLARE @currDate char(10),@om_nCount INT,@of_nCount INT				
SET @currDate = CONVERT(CHAR(8),GETDATE(),112) 
						   + RIGHT('00' + CONVERT(VARCHAR(2),DATEPART(hh,GETDATE())),2) 

IF EXISTS(SELECT * FROM RANKING_DBF.dbo.RANKING_TBL WHERE s_date = @currDate and serverindex = @iserverindex)
BEGIN
	DELETE RANKING_DBF.dbo.RANKING_TBL WHERE s_date = @currDate and serverindex = @iserverindex
END

BEGIN
INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R1',s_date = @currDate,-- m_Title = '????',
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
		AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nWinPoint DESC,m_nWin DESC

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R2',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
       AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nWin DESC,CreateTime

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,

				m_szGuild,

				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R3',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
       AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nLose DESC,m_nSurrender DESC

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R4',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
       AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nSurrender DESC,m_nLose DESC

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R5',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
       AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_MaximumUnity DESC,CreateTime

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R6',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),				
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
      	AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nGuildGold DESC,CreateTime

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R7',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
       AND A.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_AvgLevel DESC,CreateTime

INSERT RANKING_DBF.dbo.RANKING_TBL
		(
				Gu,	s_date,
				serverindex,
				m_dwLogo,
				m_idGuild,
				m_szGuild,
				m_szName,
				m_nWin,
				m_nLose,
				m_nSurrender,
				m_MaximumUnity,
				m_AvgLevel,
				m_nGuildGold,
				m_nWinPoint,
				m_nPlayTime,
				CreateTime
		)
SELECT TOP 20 Gu = 'R8',s_date = @currDate,
				serverindex = MAX(A.serverindex),
				m_dwLogo = MAX(A.m_dwLogo),
				B.m_idGuild,
				m_szGuild = MAX(A.m_szGuild),
				m_szName = MAX(C.m_szName),
				m_nWin = MAX(A.m_nWin),
				m_nLose = MAX(A.m_nLose)+	MAX(A.m_nSurrender),
				m_nSurrender = MAX(A.m_nSurrender),
				m_MaximumUnity = CONVERT(REAL,MAX(A.m_nWin)-(COUNT(B.m_nSurrender)/MAX(A.m_nLevel))),
				m_AvgLevel = ISNULL(AVG(E.m_nLevel),0),
				m_nGuildGold = ISNULL(MAX(A.m_nGuildGold),0),
				m_nWinPoint = ISNULL(MAX(A.m_nWinPoint),0),
				m_nPlayTime = ISNULL(SUM(E.TotalPlayTime),0),
				CreateTime = MAX(A.CreateTime)
    FROM CHARACTER_01_DBF.dbo.GUILD_TBL A,CHARACTER_01_DBF.dbo.GUILD_MEMBER_TBL B,CHARACTER_01_DBF.dbo.CHARACTER_TBL C,ACCOUNT_DBF.dbo.ACCOUNT_TBL_DETAIL D,CHARACTER_01_DBF.dbo.CHARACTER_TBL E
  WHERE A.m_idGuild = B.m_idGuild
      	AND C.serverindex = E.serverindex
		AND C.m_idPlayer =  E.m_idPlayer
       AND B.m_nMemberLv = 0
       AND B.m_idPlayer = C.m_idPlayer
		AND A.serverindex = B.serverindex
		AND B.serverindex = C.serverindex
		AND C.serverindex = @iserverindex
		AND C.account = D.account
  GROUP BY B.m_idGuild
ORDER BY m_nPlayTime DESC,CreateTime
END

BEGIN
	SET NOCOUNT ON;
	SELECT * FROM dbo.RANKING_TBL
    WHERE	Gu = @Gu
    	AND serverindex = @iserverindex
END
GO