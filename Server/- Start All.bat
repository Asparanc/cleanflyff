@echo off
start "AccountServer" "1. Account.exe"
start "Certifier" "2. Certifier.exe"
start "DatabaseServer" "3. Database.exe"
start "CoreServer" "4. Core.exe"
start "LoginServer" "5. Login.exe"
start "CacheServer" "6. Cache.exe"
start "WorldServer" "7. World.exe"