Rmdir /S "Client\"
Mkdir "Client\"
Mkdir "Client\World\"
Mkdir "Client\Model\"
Mkdir "Client\Char\"
Mkdir "Client\Client\"
Mkdir "Client\Icon\"
Mkdir "Client\Item\"
Mkdir "Client\Music\"
Mkdir "Client\Sound\"
Mkdir "Client\SFX\"
Mkdir "Client\Theme\"
Mkdir "Client\Weather\"
XCOPY "Resource\ResClient\" "Client\" /E/H
XCOPY "Resource\Global\World\" "Client\World\" /E/H
XCOPY "Resource\Global\Model\" "Client\Model\" /E/H
XCOPY "Resource\Global\Char\" "Client\Char\" /E/H
XCOPY "Resource\Global\Client\" "Client\Char\" /E/H
XCOPY "Resource\Global\Icon\" "Client\Icon\" /E/H
XCOPY "Resource\Global\Item\" "Client\Item\" /E/H
XCOPY "Resource\Global\Music\" "Client\Music\" /E/H
XCOPY "Resource\Global\Sound\" "Client\Sound\" /E/H
XCOPY "Resource\Global\SFX\" "Client\SFX\" /E/H
XCOPY "Resource\Global\Theme" "Client\Theme\" /E/H
XCOPY "Resource\Global\Weather" "Client\Weather\" /E/H
COPY "Source\Output\Neuz\NoGameguard\Neuz.exe" "Client\Neuz.exe"
pause