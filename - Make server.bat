@echo off
COPY "Source\Output\AccountServer\Release\AccountServer.exe" "Server\1. Account.exe"
COPY "Source\Output\CacheServer\Release\CacheServer.exe" "Server\6. Cache.exe"
COPY "Source\Output\Certifier\Release\Certifier.exe" "Server\2. Certifier.exe"
COPY "Source\Output\CoreServer\Release\CoreServer.exe" "Server\4. Core.exe"
COPY "Source\Output\DatabaseServer\Release\DatabaseServer.exe" "Server\3. Database.exe"
COPY "Source\Output\LoginServer\Release\LoginServer.exe" "Server\5. Login.exe"
COPY "Source\Output\WorldServer\Release\WorldServer.exe" "Server\7. World.exe"